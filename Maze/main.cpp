#include "Engine/GraphicEngine.h"
#include <chrono>
#include <string>
#include "App/Maze.h"

using namespace std;


class Demo : public GraphicEngine 
{
protected:
    virtual void OnUpdate(seconds elapsedTime) override
    {
        //DrawRectangle(Vec2d(), GetScreenWidth(), GetScreenHeight());
        //DrawString(Vec2d(3, 3), L"Hello World");
        DrawString(Vec2d(20, 3), std::to_wstring(elapsedTime));
        DrawCircle({ 5,5 }, 5, PIXEL_TYPE::THREEQUARTERS, PIXEL_COLOR::FG_DARK_RED);
        
        DrawCircle({ 9,9 }, 9, PIXEL_TYPE::HALF, PIXEL_COLOR::FG_DARK_CYAN);
        DrawCircle({ 9,9 }, 8);
        DrawCircle({ 30,10 }, 8, PIXEL_TYPE::SOLID, PIXEL_COLOR::GREEN);
        //DrawFullCircle({ 30,10 }, 8, PIXEL_TYPE::PIXEL_SOLID, PIXEL_COLOR::FG_GREEN | PIXEL_COLOR::BG_RED);
        //DrawLine(Vec2d(0, 1), Vec2d(6, 4), PIXEL_COLOR::FG_DARK_RED);
        //DrawLine(Vec2d(5, 5), Vec2d(1, 4), PIXEL_COLOR::FG_DARK_RED);
        //DrawLine(Vec2d(10, 10), Vec2d(15, 10), PIXEL_COLOR::FG_DARK_RED);
        //DrawLine(Vec2d(11, 11), Vec2d(10, 15), PIXEL_COLOR::FG_DARK_RED);

        Draw({ 20, 20 }, PIXEL_TYPE::HALF, PIXEL_COLOR::FG_WHITE);
        Draw({ 20, 21 }, PIXEL_TYPE::QUARTER, PIXEL_COLOR::FG_WHITE);
        Draw({ 20, 22 }, PIXEL_TYPE::SOLID, PIXEL_COLOR::FG_WHITE);
        Draw({ 20, 23 }, PIXEL_TYPE::THREEQUARTERS, PIXEL_COLOR::FG_WHITE);

        DrawTriangle({ 20, 20 }, { 20, 36 }, { 36, 36 });
    }

    virtual void OnCreate() override
    {
    }
};

int main()
{
    /*int height = 380 / 4;
    int width = 40;
    int pixelHeight = 12;
    int pixelWidth = 12;

    Demo demo;
    demo.SetUp(height, width, pixelHeight, pixelWidth);
    demo.Run();*/

    int height = 380 / 4;
    int width = 40;
    int pixelHeight = 12;
    int pixelWidth = 12;

    Maze maze;
    maze.SetUp(height, width, pixelHeight, pixelWidth);
    maze.Run();
        
	return 0;
}

