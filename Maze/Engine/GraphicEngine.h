#pragma once
#include <Windows.h>
#include <thread>
#include <chrono>
#include <functional>
#include <iostream>
#include "Common.h"

class GraphicEngine
{
public:
	GraphicEngine();

	void SetUp(int screenWidth, int screenHeight, int pixelHeight, int pixelWidth);

	const int& GetScreenWidth() const { return this->screenWidth; }
	const int& GetScreenHeight() const { return this->screenHeight; }
	const int& GetPixelWidth() const { return this->pixelWidth; }
	const int& GetPixelHeight() const { return this->pixelHeight; }

	void Run();

	void Pause(milliseconds waitTime);

protected:
	std::wstring appName;

	void Draw(int x, int y, 
		PIXEL_TYPE type = PIXEL_TYPE::SOLID, 
		PIXEL_COLOR color = PIXEL_COLOR::WHITE);

	void Draw(const Vec2d& position,
		PIXEL_TYPE type = PIXEL_TYPE::SOLID,
		PIXEL_COLOR color = PIXEL_COLOR::WHITE);
	
	void DrawRectangle(const Vec2d& position, int width, int height, 
		PIXEL_TYPE type = PIXEL_TYPE::SOLID, 
		PIXEL_COLOR foreground = PIXEL_COLOR::WHITE, 
		PIXEL_COLOR background = PIXEL_COLOR::WHITE);

	void DrawString(const Vec2d& position, std::wstring text, 
		PIXEL_COLOR foreground = PIXEL_COLOR::FG_WHITE, 
		PIXEL_COLOR background = PIXEL_COLOR::BG_BLACK);

	void DrawLine(const Vec2d& pos1, const Vec2d& pos2, 
		PIXEL_TYPE type, PIXEL_COLOR color);

	void DrawCircle(const Vec2d& pos, int radius,
		PIXEL_TYPE type = PIXEL_TYPE::SOLID,
		PIXEL_COLOR borderColor = PIXEL_COLOR::WHITE,
		PIXEL_COLOR fill = PIXEL_COLOR::WHITE);

	void DrawTriangle(const Vec2d& pos1, const Vec2d& pos2, const Vec2d& pos3,
		PIXEL_TYPE type = PIXEL_TYPE::SOLID, 
		PIXEL_COLOR color = PIXEL_COLOR::WHITE);

	void ClearScreen();

	virtual void OnCreate() = 0;
	virtual void OnUpdate(seconds elapsedTime) = 0;

private:
	int screenWidth;
	int screenHeight;
	int pixelWidth;
	int pixelHeight;
	HANDLE consoleOut;
	HANDLE consoleIn;
	SMALL_RECT windowRect;
	CHAR_INFO* screenBuffer;

	static constexpr size_t keyboardSize = 256;
	Key keyboard[keyboardSize];

	void Render();
	void DrawLineLow(const Vec2d& pos1, const Vec2d& pos2, PIXEL_TYPE type, PIXEL_COLOR color);
	void DrawLineHigh(const Vec2d& pos1, const Vec2d& pos2, PIXEL_TYPE type, PIXEL_COLOR color);
	void Loop();
	void HandleKeyboardInputs();
	GAME_MENU HandleEscapeMenu();
};

