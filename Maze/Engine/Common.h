#pragma once
#include "DataTypes/PixelColor.h"
#include "DataTypes/PixelType.h"
#include "DataTypes/Vec2d.h"
#include "DataTypes/KeyTypes.h"

typedef Vec2d_generic<int> Vec2d;
typedef float seconds;
typedef uint32_t milliseconds;

enum class GAME_MENU : uint8_t
{
	CONTINUE,
	MENU_ACTIVE,
	EXIT
};

