#include "GraphicEngine.h"
#include <string>

GraphicEngine::GraphicEngine()
{
    this->screenWidth = 0;
    this->screenHeight = 0;
    this->pixelHeight = 0;
    this->pixelWidth = 0;
    this->screenBuffer = 0;
    this->appName = L"GraphicEngine";

    consoleOut = GetStdHandle(STD_OUTPUT_HANDLE);
    consoleIn = GetStdHandle(STD_INPUT_HANDLE);
    windowRect = { 0, 0, 1, 1 };
    SetConsoleWindowInfo(consoleOut, TRUE, &windowRect);

    for (int i = 0; i < keyboardSize; i++) 
    {
        keyboard[i] = {(KEY_CODE)i, KEY_STATE::INITIAL };
    }
}

void GraphicEngine::SetUp(int screenWidth, int screenHeight, int pixelHeight, int pixelWidth)
{
    this->screenWidth = screenWidth;
    this->screenHeight = screenHeight;
    this->pixelHeight = pixelHeight;
    this->pixelWidth = pixelWidth;

    COORD coord = { (short)screenWidth, (short)screenHeight };
    SetConsoleScreenBufferSize(consoleOut, coord);
    SetConsoleActiveScreenBuffer(consoleOut);

    CONSOLE_FONT_INFOEX cfi;
    cfi.cbSize = sizeof(cfi);
    cfi.nFont = 0;
    cfi.dwFontSize.X = pixelWidth;
    cfi.dwFontSize.Y = pixelHeight;
    cfi.FontFamily = FF_DONTCARE;
    cfi.FontWeight = FW_NORMAL;
    wcscpy_s(cfi.FaceName, L"Consolas");
    SetCurrentConsoleFontEx(consoleOut, false, &cfi);

    windowRect = { 0, 0, (short)(screenWidth - 1), (short)(screenHeight - 1) };
    SetConsoleWindowInfo(consoleOut, TRUE, &windowRect);
    SetConsoleMode(consoleIn, ENABLE_EXTENDED_FLAGS | ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT);

    screenBuffer = new CHAR_INFO[(size_t)screenWidth * screenHeight];
    memset(screenBuffer, 0, sizeof(CHAR_INFO) * screenWidth * screenHeight);

    OnCreate();
}

void GraphicEngine::Draw(int x, int y, PIXEL_TYPE type, PIXEL_COLOR color)
{
    if (x >= 0 && x < screenWidth && y >= 0 && y < screenHeight)
    {
        screenBuffer[y * screenWidth + x].Char.UnicodeChar = (WCHAR)type;
        screenBuffer[y * screenWidth + x].Attributes = (WORD)color;
    }
}

void GraphicEngine::Draw(const Vec2d& position, PIXEL_TYPE type, PIXEL_COLOR color)
{
    Draw(position.X(), position.Y(), type, color);
}

void GraphicEngine::DrawRectangle(const Vec2d& position, int width, int height, 
    PIXEL_TYPE type, PIXEL_COLOR foreground, PIXEL_COLOR background)
{
    for (int y = 0; y < height; y++) 
    {
        for (int x = 0; x < width; x++)
        {
            if (y == 0 || y == height - 1 ||
                x == 0 || x == width - 1) 
            {
                Draw(Vec2d( x, y ) + position, type, foreground);
            }
            else 
            {
                Draw(Vec2d(x, y) + position, type, background);
            }
        }
    }
}

void GraphicEngine::DrawString(const Vec2d& position, std::wstring text, 
    PIXEL_COLOR foreground, PIXEL_COLOR background)
{
    PIXEL_COLOR color = (PIXEL_COLOR)((uint16_t)foreground | (uint16_t)background);

    for (size_t i = 0; i < text.size(); i++)
    {
        Vec2d pos = Vec2d((int)i, 0) + position;
        Draw(pos, (PIXEL_TYPE)text[i], color);
    }
}

void GraphicEngine::DrawLine(const Vec2d& pos1, const Vec2d& pos2, PIXEL_TYPE type, PIXEL_COLOR color)
{
    if (abs(pos2.Y() - pos1.Y()) < abs(pos2.X() - pos1.X())) 
    {
        if (pos1.X() > pos2.X()) 
        {
            DrawLineLow(pos2, pos1, type, color);
        }
        else 
        {
            DrawLineLow(pos1, pos2, type, color);
        }
    }
    else 
    {
        if (pos1.Y() > pos2.Y()) 
        {
            DrawLineHigh(pos2, pos1, type, color);
        }
        else 
        {
            DrawLineHigh(pos1, pos2, type, color);
        }
    }
}

void GraphicEngine::DrawCircle(const Vec2d& pos, int radius, PIXEL_TYPE type, 
    PIXEL_COLOR borderColor, PIXEL_COLOR fill)
{
    int d = -radius;
    int x = radius;
    int y = 0;

    while (y <= x)
    {
        Draw(Vec2d(x, y) + pos, type, fill);
        Draw(Vec2d(-x, y) + pos, type, fill);
        Draw(Vec2d(x, -y) + pos, type, fill);
        Draw(Vec2d(-x, -y) + pos, type, fill);
        Draw(Vec2d(y, x) + pos, type, fill);
        Draw(Vec2d(-y, x) + pos, type, fill);
        Draw(Vec2d(y, -x) + pos, type, fill);
        Draw(Vec2d(-y, -x) + pos, type, fill);

        if (borderColor != fill)
        {
            // Override colors
            DrawLine(Vec2d(pos) + Vec2d(-x, y), Vec2d(pos) + Vec2d(x, y) + Vec2d(1, 0), type, borderColor);
            DrawLine(Vec2d(pos) + Vec2d(-y, x), Vec2d(pos) + Vec2d(y, x) + Vec2d(1, 0), type, borderColor);
            DrawLine(Vec2d(pos) - Vec2d(-x, y) + Vec2d(1, 0), Vec2d(pos) - Vec2d(x, y), type, borderColor);
            DrawLine(Vec2d(pos) - Vec2d(-y, x) + Vec2d(1, 0), Vec2d(pos) - Vec2d(y, x), type, borderColor);
        }

        d = d + 2 * y + 1;
        y++;
        if (d > 0)
        {
            x--;
            d = d - 2 * x;
        }
    }
}

void GraphicEngine::DrawTriangle(const Vec2d& pos1, const Vec2d& pos2, 
    const Vec2d& pos3, PIXEL_TYPE type, PIXEL_COLOR color)
{
    DrawLine(pos1, pos2, type, color);
    DrawLine(pos2, pos3, type, color);
    DrawLine(pos3, pos1, type, color);

    // Draw missing pixel
    Draw(pos3, type, color);
}

void GraphicEngine::Render()
{
    WriteConsoleOutput(consoleOut, screenBuffer, { (short)screenWidth, (short)screenHeight }, { 0,0 }, &windowRect);
}

void GraphicEngine::Run()
{
    auto t = std::thread(&GraphicEngine::Loop, this);
    t.join();
}

void GraphicEngine::Pause(milliseconds waitTime)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(waitTime));
}

void GraphicEngine::DrawLineLow(const Vec2d& pos1, const Vec2d& pos2, PIXEL_TYPE type, PIXEL_COLOR color)
{
    int x1 = pos1.X();
    int y1 = pos1.Y();
    int x2 = pos2.X();
    int y2 = pos2.Y();

    int dx = x2 - x1;
    int dy = y2 - y1;
    int yi = 1;

    if (dy < 0)
    {
        yi = -1;
        dy = -dy;
    }
    int D = 2 * dy - dx;
    int y = y1;

    for (int x = x1; x < x2; x++)
    {
        Draw(x, y, type, color);
        if (D > 0)
        {
            y += yi;
            D = D + (2 * (dy - dx));
        }
        else
        {
            D = D + 2 * dy;
        }
    }
}

void GraphicEngine::DrawLineHigh(const Vec2d& pos1, const Vec2d& pos2, PIXEL_TYPE type, PIXEL_COLOR color)
{
    int x1 = pos1.X();
    int y1 = pos1.Y();
    int x2 = pos2.X();
    int y2 = pos2.Y();

    int dx = x2 - x1;
    int dy = y2 - y1;
    int xi = 1;

    if (dx < 0)
    {
        xi = -1;
        dx = -dx;
    }
    int D = 2 * dx - dy;
    int x = x1;

    for (int y = y1; y < y2; y++)
    {
        Draw(x, y, type, color);
        if (D > 0)
        {
            x += xi;
            D = D + (2 * (dx - dy));
        }
        else
        {
            D = D + 2 * dx;
        }
    }
}

void GraphicEngine::Loop()
{
    using clock = std::chrono::steady_clock;

    auto tp1 = clock::now();
    auto tp2 = clock::now();

    std::chrono::duration<float> elapsedTime;

    GAME_MENU game = GAME_MENU::CONTINUE;
    do
    {
        tp2 = clock::now();
        elapsedTime = tp2 - tp1;
        tp1 = tp2;

        ClearScreen();
        HandleKeyboardInputs();
        
        if (keyboard[(size_t)KEY_CODE::ESC].CurrentState == KEY_STATE::PRESSED ||
            game == GAME_MENU::MENU_ACTIVE) 
        {
            if (keyboard[(size_t)KEY_CODE::ESC].PreviousState != KEY_STATE::PRESSED &&
                keyboard[(size_t)KEY_CODE::ESC].CurrentState == KEY_STATE::PRESSED) 
            {
                // Pause the clock....
            }

            game = HandleEscapeMenu();
        }
        else 
        {
            OnUpdate(elapsedTime.count());
        }

        Render();

        wchar_t s[256];
        swprintf_s(s, 256, L"Console Game Engine - %s - FPS: %3.2f", appName.c_str(), 1.0f / elapsedTime.count());
        SetConsoleTitle(s);

    } while (game != GAME_MENU::EXIT);
}

void GraphicEngine::ClearScreen()
{
    DrawRectangle({ 0, 0 }, GetScreenWidth(), GetScreenHeight(), 
        PIXEL_TYPE::SOLID, PIXEL_COLOR::FG_BLACK, PIXEL_COLOR::FG_BLACK);
}

void GraphicEngine::HandleKeyboardInputs()
{
    int screenIndexX = 0;

    for (int i = 0; i < keyboardSize; i++)
    {
        Key* key = &keyboard[i];
        key->PreviousState = key->CurrentState;

        uint16_t keyState = GetAsyncKeyState(i);
        if (keyState & 0x8000)
        {
            if (key->CurrentState == KEY_STATE::PRESSED || key->CurrentState == KEY_STATE::HELD)
            {
                key->CurrentState = KEY_STATE::HELD;
            }
            else
            {
                key->CurrentState = KEY_STATE::PRESSED;
            }
        }
        else
        {
            if (key->CurrentState == KEY_STATE::PRESSED || key->CurrentState == KEY_STATE::HELD)
            {
                key->CurrentState = KEY_STATE::RELEASED;
            }
            else
            {
                key->CurrentState = KEY_STATE::NOT_PRESSED;
            }
        }

        // DEBUG
        if (key->KeyCode >= KEY_CODE::A && key->KeyCode <= KEY_CODE::Z)
        {
            DrawString({ screenIndexX * 2 + 1, GetScreenHeight() - 2 }, { (wchar_t)key->KeyCode });
            DrawString({ screenIndexX * 2 + 1, GetScreenHeight() - 1 }, std::to_wstring((uint8_t)key->CurrentState));

            screenIndexX++;
        }
    }
}

GAME_MENU GraphicEngine::HandleEscapeMenu()
{
    // default size of menu
    // TODO: What happens if pixel size is too low
    const int width = 15; 
    const int height = 7;

    enum class MENU {
        OK,
        CANCEL
    };

    static MENU menu = MENU::OK;

    PIXEL_COLOR textOK = PIXEL_COLOR::FG_BLACK;
    PIXEL_COLOR textCancel = PIXEL_COLOR::FG_BLACK;

    if (keyboard[(size_t)KEY_CODE::LEFT].CurrentState == KEY_STATE::PRESSED
        || menu == MENU::OK)
    {
        menu = MENU::OK;
        textOK = PIXEL_COLOR::BG_DARK_GREEN;
        textCancel = PIXEL_COLOR::FG_BLACK;
    }
    
    if (keyboard[(size_t)KEY_CODE::RIGHT].CurrentState == KEY_STATE::PRESSED
        || menu == MENU::CANCEL)
    {
        menu = MENU::CANCEL;
        textOK = PIXEL_COLOR::FG_BLACK;
        textCancel = PIXEL_COLOR::BG_DARK_GREEN;
    }

    
    // Check if Enter key has been pressed and return based on user decision
    if (keyboard[(size_t)KEY_CODE::ENTER].CurrentState == KEY_STATE::PRESSED) 
    {
        if (menu == MENU::CANCEL) 
        {
            menu = MENU::OK; // reset internal menu state
            return GAME_MENU::CONTINUE;
        }
        else if (menu == MENU::OK) 
        {
            return GAME_MENU::EXIT;
        }
    }
    else
    {
        // Calculate start position of menu based on screen size and menu size
        Vec2d menuPos = { (GetScreenWidth() - width) / 2 , (GetScreenHeight() - height) / 2 };
        DrawRectangle(menuPos, width, height, PIXEL_TYPE::SOLID,
            PIXEL_COLOR::WHITE, PIXEL_COLOR::BLACK);

        // Draw menu text 
        DrawString(menuPos + Vec2d(width / 2 - 2, 2), L"Exit?", PIXEL_COLOR::FG_WHITE, PIXEL_COLOR::FG_BLACK);
        DrawString(menuPos + Vec2d(2, height / 2 + 1), L"Ok", PIXEL_COLOR::FG_WHITE, textOK);
        DrawString(menuPos + Vec2d(4 + 2, height / 2 + 1), L"Cancel", PIXEL_COLOR::FG_WHITE, textCancel);
    }

    return GAME_MENU::MENU_ACTIVE;
}
