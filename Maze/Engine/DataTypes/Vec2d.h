#pragma once

template<typename NumericType>
class Vec2d_generic 
{
public:
	Vec2d_generic() 
		: x(0), y(0) { }
	
	Vec2d_generic(NumericType _x, NumericType _y)
		: x(_x), y(_y) { }

	Vec2d_generic(Vec2d_generic<NumericType> const& origin)
		: Vec2d_generic<NumericType>(origin.x, origin.y) { }

	NumericType& X() { return x; }
	const NumericType& X() const { return x; }

	NumericType& Y() { return y; }
	const NumericType& Y() const { return y; }
	
	Vec2d_generic<NumericType> operator+(const Vec2d_generic<NumericType>& rhs)
	{
		return { this->x + rhs.x, this->y + rhs.y };
	}

	Vec2d_generic<NumericType> operator-(const Vec2d_generic<NumericType>& rhs)
	{
		return { this->x - rhs.x, this->y - rhs.y };
	}

	Vec2d_generic<NumericType>& operator=(const Vec2d_generic<NumericType>& rhs)
	{
		if (this == &rhs)
			return *this;

		this->x = rhs.x;
		this->y = rhs.y;

		return *this;
	}

	bool operator==(const Vec2d_generic<NumericType>& rhs) const
	{
		return this->x == rhs.x && this->y == rhs.y;
	}

	bool operator!=(const Vec2d_generic<NumericType>& rhs) const
	{
		return this->x != rhs.x || this->y != rhs.y;
	}

	NumericType Mag() 
	{
		return sqrt(x * x + y * y);
	}

	Vec2d_generic<NumericType> Norm() 
	{
		NumericType r = 1 / Mag();
		return { x * r, y * r };
	}

	NumericType Dot(const Vec2d_generic<NumericType> rhs)
	{
		return this->x * rhs.x + this->y * rhs.y;
	}

private:
	NumericType x;
	NumericType y;
};
