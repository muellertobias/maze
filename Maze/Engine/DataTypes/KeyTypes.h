#pragma once


enum class KEY_STATE : uint8_t
{
	INITIAL,
	NOT_PRESSED,
	PRESSED,
	HELD,
	RELEASED
};

enum class KEY_CODE : uint8_t
{
	UNDEFINED = 0x00,
	LEFT_MOUSE = 0x01,
	RIGHT_MOUSE,
	TAB = 0x09,
	ENTER = 0x0d,
	SHIFT = 0x10,
	CTRL = 0x11,
	ALT = 0x12,
	ESC = 0x1b,
	SPACE = 0x20,
	LEFT = 0x25,
	UP,
	RIGHT,
	DOWN,
	ZERO = 0x30,
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	A = 0x41,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z,
	F1 = 0x70,
	F2,
	F3,
	F4,
	F5,
	F6,
	F7,
	F8,
	F9,
	F10,
	F11,
	F12
};

struct Key
{
	KEY_CODE KeyCode;
	KEY_STATE CurrentState;
	KEY_STATE PreviousState;
};
