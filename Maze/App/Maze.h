#pragma once
#include "../Engine/GraphicEngine.h"
#include <random>
#include <stack>

enum MAZE_CELL : uint8_t
{
	INITIAL = 0x00,
	NORTH = 0x01,
	WEST = 0x02,
	SOUTH = 0x04,
	EAST = 0x08,
	VISITED = 0x10
};

struct Cell 
{
	MAZE_CELL State;

};

class Maze : public GraphicEngine
{
public:
	Maze() : GraphicEngine() 
	{	
		appName = L"Maze";
	}

protected:
	virtual void OnUpdate(seconds elapsedTime) override;
	void DrawMaze();
	virtual void OnCreate() override;

	void DrawMazeCell(Vec2d pos, PIXEL_COLOR color = ROOM_COLOR);

	MAZE_CELL GetCellInfo(Vec2d pos) 
	{
		MAZE_CELL cell = maze[width * pos.Y() + pos.X()];
		return cell;
	}

	void SetCellInfo(Vec2d pos, MAZE_CELL info) 
	{
		maze[width * pos.Y() + pos.X()] = info;
	};

	void AddCellInfo(Vec2d pos, MAZE_CELL info) 
	{
		MAZE_CELL cell = GetCellInfo(pos);
		maze[width * pos.Y() + pos.X()] = (MAZE_CELL)(cell | info);
	}

	bool IsSet(Vec2d pos, MAZE_CELL cellInfo);

private:
	const size_t WALL_SIZE = 1;
	const size_t ROOM_SIZE = 2;
	const size_t CELL_SIZE = WALL_SIZE + ROOM_SIZE;

	const static PIXEL_COLOR WALL_COLOR = PIXEL_COLOR::BLACK;
	const static PIXEL_COLOR ROOM_COLOR = PIXEL_COLOR::WHITE;
	const static PIXEL_COLOR VISITED_COLOR = PIXEL_COLOR::BLUE;
	size_t width;
	size_t height;
	MAZE_CELL* maze;

	size_t visitedCells;
	std::stack<Vec2d> stack;
};
