#include "Maze.h"

void Maze::OnUpdate(seconds elapsedTime)
{
	if (visitedCells >= width * height) 
	{
		DrawMaze();
		return;
	}
		

	std::vector<MAZE_CELL> neighbors;

	if (stack.top().X() + 1 < width && IsSet(stack.top() + Vec2d(1, 0), VISITED) == false)
	{
		neighbors.push_back(EAST);
	}
	if (stack.top().Y() + 1 < height && IsSet(stack.top() + Vec2d(0, 1), VISITED) == false)
	{
		neighbors.push_back(SOUTH);
	}
	if (stack.top().X() - 1 < width && IsSet(stack.top() + Vec2d(-1, 0), VISITED) == false)
	{
		neighbors.push_back(WEST);
	}
	if (stack.top().Y() - 1 < height && IsSet(stack.top() + Vec2d(0, -1), VISITED) == false)
	{
		neighbors.push_back(NORTH);
	}

	if (neighbors.empty()) 
	{
		stack.pop();
	}
	else
	{
		MAZE_CELL direction = neighbors[rand() % neighbors.size()];

		Vec2d next;
		switch (direction)
		{
		case NORTH:
			next = Vec2d(stack.top() + Vec2d(0, -1));
			AddCellInfo(stack.top(), NORTH);
			SetCellInfo(next, SOUTH);
			break;
		case WEST:
			next = stack.top() + Vec2d(-1, 0);
			AddCellInfo(stack.top(), WEST);
			SetCellInfo(next, EAST);
			break;
		case SOUTH:
			next = stack.top() + Vec2d(0, 1);
			AddCellInfo(stack.top(), SOUTH);
			SetCellInfo(next, NORTH);
			break;
		case EAST:
			next = stack.top() + Vec2d(1, 0);
			AddCellInfo(stack.top(), EAST);
			SetCellInfo(next, WEST);
			break;
		}
		AddCellInfo(next, VISITED);
		stack.push(next);
		visitedCells++;
	}

	DrawMaze();
	DrawMazeCell(stack.top(), VISITED_COLOR);
	Pause(10);
}

void Maze::DrawMaze()
{
	for (int x = 0; x < width; x++)
	{
		for (int y = 0; y < height; y++)
		{
			DrawMazeCell({ x, y });
		}
	}
}

void Maze::OnCreate()
{
	width = GetScreenWidth() / CELL_SIZE;
	height = GetScreenHeight() / CELL_SIZE;
	maze = new MAZE_CELL[width * height];

	memset(maze, (int)MAZE_CELL::INITIAL, width * height);
	
	visitedCells = 1;
	stack.push(Vec2d(0, 0));
	SetCellInfo(stack.top(), VISITED);
}

void Maze::DrawMazeCell(Vec2d pos, PIXEL_COLOR color)
{
	MAZE_CELL cell = GetCellInfo(pos);

	for (int x = 0; x < CELL_SIZE; x++) 
	{
		for (int y = 0; y < CELL_SIZE; y++)
		{
			if (x < ROOM_SIZE && y < ROOM_SIZE) 
			{
				Draw(pos.X() * CELL_SIZE + x, pos.Y() * CELL_SIZE + y, PIXEL_TYPE::SOLID, color);
			}
			else 
			{
				Draw(pos.X() * CELL_SIZE + x, pos.Y() * CELL_SIZE + y, PIXEL_TYPE::SOLID, WALL_COLOR);

				if ((EAST & cell) == EAST && y < ROOM_SIZE) 
				{
					Draw(pos.X() * CELL_SIZE + x, pos.Y() * CELL_SIZE + y, PIXEL_TYPE::SOLID, ROOM_COLOR);
				}

				if ((SOUTH & cell) == SOUTH && x < ROOM_SIZE) 
				{
					Draw(pos.X() * CELL_SIZE + x, pos.Y() * CELL_SIZE + y, PIXEL_TYPE::SOLID, ROOM_COLOR);
				}
				
			}
		}
	}
}

bool Maze::IsSet(Vec2d pos, MAZE_CELL cellInfo)
{
	MAZE_CELL cell = GetCellInfo(pos);
	return (cell & cellInfo) == cellInfo;
}
